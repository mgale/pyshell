import abc
import os
import logging
log = logging.getLogger("PyShell")


class CommandExecutionPlugin(object):
    """
    Command Execution Abstract class

    To create a plugin create a subclass.
    """

    __metaclass__ = abc.ABCMeta

    def pre_run_cmd(self, args):
        """
        @param args: The command and all of the command line arguments
        Same arguments as run_cmd

        Here a plugin can alter a command or any of the arguments
        before the command is run.

        @return args: The same or a new args list, can also be None
        """

        return args

    @abc.abstractmethod
    def run_cmd(self, args):
        """
        @param args: The command and all of the command line arguments

        The first item is the command to execute, format can be passed
        directly to subprocess.call / Popen

        The run_cmd method will only be executed once, regardles of how
        many plugins exists. So the first plugin to indicate that it 
        executed the command will cause the run_cmd method to NOT be 
        executed on all other plugins. The pre_run_cmd and post_run_cmd 
        methods are always run.
        
        @return exit_status: Set the exit status for this command, this
        should only be set if the plugin ran the command. Can also be 
        None - which means the plugin did not execute the command.
        """

        raise NotImplementedError("Plugin subclass should implement this method!!!")

    def post_run_cmd(self, args, exit_result):
        """
        @param args: The command and all of the command line arguments
        @param exit_results: Is the result of the last plugin run_cmd,
        so if a Command Handler plugins run_cmd returns a failure,
        execution will stop there and exit_result will be the value
        from there.

        Here a plugin can take action after a command was run,
        maybe based on the exit status.
        """

        pass