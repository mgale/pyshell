# Load Plugin class
#
import sys
import traceback
import logging
log = logging.getLogger("PyShell")

class LoadPlugins(object):
    def __init__(self):
        self.plugin_order = []
        self.plugin_status = {}

    def load_plugin(self, plugin):
        try:
            mod, cls = plugin.split('.')
            mod = __import__('%s'%(mod))
            my_class = getattr(mod, cls)
            self.plugin_order.append(my_class)
            self.plugin_status[plugin] = True
            log.debug("Success: Loaded plugin: %s"%(plugin))
        except:
            self.plugin_status[plugin] = False
            log.exception("Failed: Loaded plugin: %s"%(plugin))

        return self.plugin_status[plugin]

    def get_plugin_status(self, plugin):
        if self.plugin_status.has_key(plugin):
            return self.plugin_status[plugin]
        else:
            log.warning("Unknown plugin: %s"%(plugin))
            return False

    def get_plugins(self):
        return self.plugin_order




