import os, select
from cStringIO import StringIO
import load_plugin
import logging
log = logging.getLogger("PyShell.CommandHandler")

# class LineReader(object):

#     def __init__(self, fd):
#         self._fd = fd
#         self._buf = ''

#     def fileno(self):
#         return self._fd

#     def readlines(self):
#         data = os.read(self._fd, 4096)
#         if not data:
#             # EOF
#             return None
#         self._buf += data
#         if '\n' not in data:
#             return []
#         tmp = self._buf.split('\n')
#         lines, self._buf = tmp[:-1], tmp[-1]
#         return lines

class CommandHandler(object):
    def __init__(self, plugin_list, log_level):
        self.plugin_list = plugin_list
        self.plugins = None

        if log_level:
            log.setLevel(log_level)

        self._load_plugins()

    def _load_plugins(self):
        self.plugins = load_plugin.LoadPlugins()
        for plug in self.plugin_list:
            os.environ["PLUGIN_%s"%(plug)] = "%s"%(self.plugins.load_plugin(plug))

    def run_cmd(self, rword_obj, args):
        plugin_objs = []
        for plug_cls in self.plugins.get_plugins():
            plug = plug_cls()
            plugin_objs.append(plug)

        if rword_obj.is_reserved(args[0]):
            rword_cls = rword_obj.get_reserved(args[0])
            rword = rword_cls()
            log.debug("Using reserved word: %s"%(args))
            plugin_objs.insert(0, rword)

        for plug in plugin_objs:
            if hasattr(plug, "pre_run_cmd"):
                new_args = plug.pre_run_cmd(args)
                if new_args:
                    args = new_args

        for plug in plugin_objs:
            exit_status = plug.run_cmd(args)
            if exit_status is not None:
                log.debug("Executed by plugin: %s"%(plug))
                break

        #We want the plugins to be follow the same path backout
        for plug in reversed(plugin_objs):
            if hasattr(plug, "post_run_cmd"):
                plug.post_run_cmd(args, exit_status)
