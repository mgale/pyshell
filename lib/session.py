# Session class designed to hold information related to this
# session only.
#
# Session variables should contain run time information and
# operation metrics
#
import uuid
import logging
log = logging.getLogger("PyShell")

class Session(object):
    def __init__(self):
        self.id = uuid.uuid4()
        self.last_job = None
        self.last_job_status = None
        self.last_job_time = None
        self.last_prompt_time = None


