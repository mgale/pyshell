# job class designed to hold information related to this
# job only.
#
#
import uuid
import logging
log = logging.getLogger("PyShell.Job")

class Job(object):
    def __init__(self):
        self.id = uuid.uuid4()
        self.job_status = None
        self.job_time = None

    def read(self):
        pass


    def send_signal(self, sig):
        pass

    