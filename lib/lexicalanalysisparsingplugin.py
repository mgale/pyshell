import abc
import os
import logging
log = logging.getLogger("PyShell")

class LexicalAnalysisParsingPlugin(object):
    """
    Lexical Analysis Parsing Abstract class

    To create a plugin create a subclass and implement the 
    parse_input method.

    This a abc.ABCMeta class
    """

    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def parse_input(self, in_stream, tokens):
        """
        @param in_stream: is a string of text received from raw_input
        @param tokens: in a list of tokens created by a previous plugin,
        so each plugin has a choice, it can alter the token list only
        or it can re-parse the raw input and create a new set of
        tokens.

        If the plugin is ok with the previous parsing it can return the
        same token list. If not it can make a new one and return it.

        An empty list should be returned on failure or if the plugin
        wants more data to be read from the input.

        @return token_list: Return a list of tokens.
        """

        raise NotImplementedError("Plugin subclass should implement this method!!!")