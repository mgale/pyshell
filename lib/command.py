#
#
# Detailed view of how it works:
#
# grep home /etc/passwd | sort -n > /tmp/user.list
#
#

class Command(object):
    def __init__(self, cmd, cmd_args):
        """
        Create a command object to hold meta data needed to run the 
        command.

        @param cmd - Is the actual command, for example `ls`
        @param cmd_args - Is a list of arguments being passed in
        like -l -a -r -t.
        @param cmd_input - Is what you are passing to the command, for
        example /tmp.

        Complete example `ls -l -a -r -t /tmp`
        """
        self.cmd = cmd
        self.cmd_args = cmd_args
        #We don't need to worry about input direction, it is always
        #overwrite.
        self.output_redirect_type = "w"
        self.output_err_redirect_type = "w"
        self.stop_on_failure = False
        #These are what we want them set to.
        self.stdin = None
        self.stdout = None
        self.stderr = None
        #These need to be updated when the command is run with the
        #actual objects
        self.cmd_stdin = None
        self.cmd_stdout = None
        self.cmd_stderr = None

    def set_stdin(self, fileobj=None):
        self.stdin = fileobj

    def set_stdout(self, fileobj=None):
        self.stdout = fileobj

    def set_stderr(self, fileobj=None):
        self.stderr = fileobj

    def set_stdout_redirect_type(self, output="w"):
        self.output_redirect_type = output

    def set_stderr_redirect_type(self, output="w"):
        self.output_err_redirect_type = output

    def get_cmd_stdin(self):
        return self.cmd_stdin

    def get_cmd_stdout(self):
        return self.cmd_stdout

    def get_cmd_stderr(self):
        return self.cmd_stderr