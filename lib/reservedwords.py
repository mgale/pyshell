import os, inspect
import load_plugin
import logging
log = logging.getLogger("PyShell.ReservedWords")

class ReservedWords(object):
    def __init__(self, plugin_list, log_level):
        self.plugin_list = plugin_list
        self.plugins = {}

        if log_level:
            log.setLevel(log_level)

        self._load_plugins()

    def _load_plugins(self):
        for mod in self.plugin_list:
            try:
                mod, cls = mod.split('.')
                mod = __import__('%s'%(mod))
                if cls == "*":
                    cls_list = [ cls for cls, modname in inspect.getmembers(
                        mod, inspect.isclass) ]
                else:
                    cls_list = [ cls ]

                for cls in cls_list:
                    my_class = getattr(mod, cls)
                    self.plugins[cls] = my_class
                    log.debug("Success: Loaded reserved word: %s.%s"%(mod, cls))
            except:
                log.exception("Failed: Loading reserved word: %s"%(mod))


    def is_reserved(self, word):
        if self.plugins.has_key(word):
            return True
        else:
            return False

    def get_reserved(self, word):
        if self.plugins.has_key(word):
            return self.plugins[word]







