import os
import load_plugin
import logging
log = logging.getLogger("PyShell.LexicalAnalysisParsing")

class LexicalAnalysisParsing(object):
    def __init__(self, plugin_list, log_level=None):
        self.plugin_list = plugin_list
        self.plugins = None

        if log_level:
            log.setLevel(log_level)

        self._load_plugins()

    def _load_plugins(self):
        self.plugins = load_plugin.LoadPlugins()
        for plug in self.plugin_list:
            result = self.plugins.load_plugin(plug)
            if result:
                os.environ["PLUGIN_%s"%(plug)] = "%s"%(result)

    def parse_input(self, in_stream):
        result_tokens = []
        for plug_cls in self.plugins.get_plugins():
            plug = plug_cls()
            tokens = plug.parse_input(in_stream, result_tokens)

            if len(tokens) > 0:
                result_tokens = tokens
                log.debug("Plugin status: %s : %s"%(plug, tokens))
            else:
                log.debug("Plugin status: %s : empty reply"%(plug))
                return []

        return result_tokens
