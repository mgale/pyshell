# Environment class designed to hold environment variables
# for the given session.
#
# This should not contain variables and information needed at
# execution time / shell startup. That information belongs in
# config.py.
#
# Here should be environment variables that a shell will need to
# function once running. All of the variables defined in config.py
# are added to the evnironment object. So they are still accessible.
#
# Environment variables can be changed at any time and are expected
# to be updated by the user.

import os
import logging
log = logging.getLogger("PyShell")

class Environment(object):
    def set(self, k, v):
        log.debug("Environment set: %s: %s"%(k, v))
        os.environ[k] = v

    def get(self, k):
        try:
            return os.environ[k]
        except KeyError:
            return None

    def unset(self, k):
        try:
            log.debug("Environment del: %s"%(k))
            del os.environ[k]
        except KeyError:
            pass

        return True

    def dump(self):
        return os.environ.items