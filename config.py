import os
import sys
import logging

BASE_DIR = os.path.dirname(os.path.realpath(__file__))
PLUGIN_DIR = "%s/plugins"%(BASE_DIR)

DEFAULT_LOG_LEVEL = logging.DEBUG

PRI_PROMPT = "Test: "
SEC_PROMPT = ">"

#PLUGINS !!!
#A large portion of functionality will be written as a plugin.
#This will allow other people to see how the software works
#and ideally write their own plugin to expand functionality.
#
#All plugins sections are defined as a list of modules.class_name and
#the plugin must exist in the plugins directory

#Lexical Analysis and Parsing
#Handles parsing the input from the command line
#This plugin is passed a list tokens it can alter and return
#Plugins are processed in a serial manner
LAP_PLUGINS = [ 'input_parsing.InputParsing' ]
LAP_PLUGINS_LOGLEVEL = logging.DEBUG

#Variable Expansion
#This area is responsible for expanding all variables used on the
#command line
VEXPAND_PLUGINS = [ ]
VEXPAND_PLUGINS_LOGLEVEL = logging.INFO

#Reserved Words
#Offers a manner to provide shortcuts and functionality, it operates
#like the CMDHANDLER_PLUGINS with the exeception that only 1 action
#can be bound to a reserved word. For example in Command Execution
#there could be 10 plugins and each would run for a system command.
#However only 1 method / reserve word can exist even if multiple
#modules and classes are used. The methods (reserverd words) are
#loaded into a unqiue list, last one in wins.
RWORDS_PLUGINS = [ 'default_reserved_words.*' ]
RWORDS_PLUGINS_LOGLEVEL = logging.INFO

#Command Execution
#This plugin will be called before, to handle after every system
#command that is executed.
#Plugins are not processed in serial manner, the pre_run method
#is processed for all plugins first, then the run method for all
#plugins and finally the post_run method for all plugins.
CMDHANDLER_PLUGINS = [ 'command_execution.CommandExecution' ]
CMDHANDLER_PLUGINS_LOGLEVEL = logging.INFO