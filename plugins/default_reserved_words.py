# The return value from a reserved_word should be the exit status
# All output should be done via the print method
import os, sys
import logging
log = logging.getLogger("PyShell")

class echo(object):
    def run_cmd(self, args):
        args.pop()
        print ' '.join([ a for a in args ])
        return 0

class cd(object):
    def run_cmd(self, args):
        if len(args) > 1:
            dst_path = args[1]
        else:
            dst_path = os.environ["HOME"]

        if not os.access(dst_path, os.F_OK):
            print "%s does not exist"%(dst_path)
            return 1

        try:
            os.chdir(dst_path)
            return 0
        except OSError:
            print "%s %s"%(dst_path, e.message)
            return 1

class pwd(object):
    def run_cmd(self, args):
        print "%s"%(os.getcwd())
        return 0

class env(object):
    def run_cmd(self, args):
        for k,v in os.environ.iteritems():
            print "%s=%s"%(k, v)
        return 0

class exit(object):
    def run_cmd(self, args):
        sys.exit(0)