# Command Execution plugin
#
#
#
import commandexecutionplugin
import subprocess
import sys
import logging
log = logging.getLogger("PyShell.CommandHandler")

class CommandExecution(commandexecutionplugin.CommandExecutionPlugin):

    def run_cmd(self, args):
        process = subprocess.Popen(args)
        process.wait()
        return process.returncode

    def pre_run_cmd(self, args):
        return super(CommandExecution, self).pre_run_cmd(args)