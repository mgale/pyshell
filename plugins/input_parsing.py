# Input Parsing plugin
import logging
import shlex
import lexicalanalysisparsingplugin
log = logging.getLogger("PyShell")

class InputParsing(lexicalanalysisparsingplugin.LexicalAnalysisParsingPlugin):

    def parse_input(self, in_stream, tokens):
        try:
            input_tokens = shlex.shlex(in_stream, posix=True)
            input_tokens.whitespace_split = True
            tokens = [ t for t in input_tokens ]
            log.debug("Token list: %s"%(tokens))
            return tokens
        except ValueError:
            return []
