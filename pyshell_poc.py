#!/usr/bin/python
#
#
# A Bash comparable shell in Python
#
# Bash is a popular shell, most likely installed on almost
# every Linux system available.
#
# However it could use some new features :)
#
# Pyshell tries to offer the same functionality and behaviour
# in order to make switching easy.
#
# Major components:
# -- Input processing
# -- Lexical Analysis and Parsing
# -- Expansions (brace, titles, variables, etc)
# -- Command Execution

import os, sys
import getopt
import termios
import readline
import logging
import imp
import config
import shlex
import signal

sys.path.append("%s/lib"%(config.BASE_DIR))
sys.path.append("%s"%(config.PLUGIN_DIR))

import session
import lexicalanalysisparsing
import commandhandler
import reservedwords

import array
import fcntl
import pty
import select
import signal
import termios
import tty

def usage():
    usage = """
USAGE: pyshell.py [Options]

Options:
    -h, --help                  This menu ...
    -d, --debug                 Enable debugging
    -v, --verbose               Enable verbose logging

    -c, --config-file=          Configuration file to use
"""

    return usage

def parse_cmd_line(argv):
    """
    Parse command line arguments

    argv: Pass in cmd line arguments
    """

    short_args = "dvh"
    long_args = ("debug",
                    "verbose",
                    "help",
                    )
    try:
        opts, extra_opts = getopt.getopt(argv[1:], short_args, long_args)
    except getopt.GetoptError, e:
        print "Unrecognized command line option or missing required argument: %s" %(e)
        print usage()
        sys.exit(253)

    cmd_line_option_list = {}
    cmd_line_option_list['DEBUG'] = False
    cmd_line_option_list['VERBOSE'] = False

    for opt, val in opts:
        if (opt in ("-h", "--help")):
            print usage()
            sys.exit(0)
        elif (opt in ("-d", "--debug")):
            cmd_line_option_list["DEBUG"] = "true"
        elif (opt in ("-v", "--verbose")):
            cmd_line_option_list["VERBOSE"] = "true"

    return cmd_line_option_list


def main():


    cmd_options = parse_cmd_line(sys.argv)

    #Setup logging
    log = logging.getLogger("PyShell")
    log_level = config.DEFAULT_LOG_LEVEL

    if cmd_options['VERBOSE'] == True:
        log_level = logging.INFO

    if cmd_options['DEBUG'] == True:
        log_level = logging.DEBUG

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    
    console_log = logging.StreamHandler()
    console_log.setLevel(logging.DEBUG)
    console_log.setFormatter(formatter)
    log.setLevel(log_level)
    log.addHandler(console_log)

    #Populate initial environment
    log.debug("Setting up environment")
    for c in dir(config):
        if c == "__builtins__":
            break
        os.environ[c] = str(config.__dict__[c]).strip('[]')

    #Not sure if I need this yet
    #os.environ["OLD_TERM_ATTR"] = str(termios.tcgetattr(sys.stdin.fileno()).strip('[]')

    sess = session.Session()

    run = True
    current_stream = ''
    input_eof = True

    sess.lap_obj = lexicalanalysisparsing.LexicalAnalysisParsing(
        config.LAP_PLUGINS, config.LAP_PLUGINS_LOGLEVEL)

    sess.cmdhandler_obj = commandhandler.CommandHandler(
        config.CMDHANDLER_PLUGINS, config.CMDHANDLER_PLUGINS_LOGLEVEL)

    sess.rword_obj = reservedwords.ReservedWords(
        config.RWORDS_PLUGINS, config.RWORDS_PLUGINS_LOGLEVEL)

    while run:
        try:
            if input_eof:
                prompt = os.environ["PRI_PROMPT"]
                current_stream = raw_input(prompt)
            else:
                prompt = os.environ["SEC_PROMPT"]
                in_stream = raw_input(prompt)
                current_stream+=in_stream
        except EOFError:
            run = False
            print ""
            break

        except KeyboardInterrupt:
            input_eof = True
            print ""
            continue

        if len(current_stream) == 0:
            continue

        tokens = sess.lap_obj.parse_input(current_stream)

        if len(tokens) > 0:
            input_eof = True
        else:
            input_eof = False
            continue


        #sess.cmdhandler_obj.run_cmd(sess.rword_obj, tokens)
        


if (__name__ == '__main__'):
    result = main()
    sys.exit(result)
